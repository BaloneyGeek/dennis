dennisng
========

I solemnly swear I'm up to no good

Dennis-NG is currently a UDP to TCP DNS Proxy. The original Dennis was coded using pure C++11 and was simply a quick proxy; NG is a re-write using Qt5 and intends to be a full-blown DNS cache some day, probably natively supporting DNSCrypt.

To install, build and run as superuser. You need Qt5-Core and Qt5-Network. You'll also need to place the included dennis.json file in /etc and the systemd service file in 
/etc/systemd/system. You can use the PKGBUILD to build an Arch Package, just don't put it up on the AUR just yet.

The original dennis was blazingly fast, perfroming resolutions over TCP in under 200msec. Dennis-NG is much slower, averaging around a second. I'd recommend putting a cache (like dnsmasq) in front of Dennis-NG for now; eventually Dennis-NG will include its own cache.

Distros: Don't package this just yet. There'll be major changes.
Users: You have been warned.
