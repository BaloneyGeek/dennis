cmake_minimum_required(VERSION 3.5.0)
include(FeatureSummary)

project(DennisProxy)
find_package(Qt5 5.6.0 CONFIG REQUIRED Core Network)

add_subdirectory(Sources)
add_subdirectory(ConfigFiles)

feature_summary(WHAT ALL INCLUDE_QUIET_PACKAGES FATAL_ON_MISSING_REQUIRED_PACKAGES)
