// Copyright 2014 Boudhayan Gupta <me@BaloneyGeek.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "TCPServer.h"

TCPConnection::TCPConnection()
{
    socket = NULL;
    upstream_request = NULL;
    request_size = 0;
}

TCPServer::TCPServer(DConfig * cfg, QObject * parent) : QObject(parent)
{
    config = cfg;

    server = new QTcpServer(this);
    if (!(server->listen(config->host(), config->port()))) {
        qFatal("ERROR: Failed to bind TCP server to port %d on server %s", config->port(), qPrintable(config->host().toString()));
    }
    connect(server, SIGNAL(newConnection()), this, SLOT(newConnection()));
    connect(server, SIGNAL(acceptError(QAbstractSocket::SocketError)), this, SLOT(acceptError()));

    connect (this, SIGNAL(haveRequest(QTcpSocket*)), this, SLOT(sendRequest(QTcpSocket*)));
    connect (this, SIGNAL(haveResponse(QTcpSocket*)), this, SLOT(sendResponse(QTcpSocket*)));

    qDebug() << "Listening for TCP connections on port" << config->port() << "on IP" << qPrintable(config->host().toString());
}

TCPServer::~TCPServer()
{
    server->close();
    server->disconnect();
    server->deleteLater();
}

void TCPServer::newConnection()
{
    QTcpSocket * socket = server->nextPendingConnection();
    TCPConnection * conn = new TCPConnection();

    conn->socket = socket;
    active_connections[socket] = conn;

    connect(socket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(socketError(QAbstractSocket::SocketError)));
    connect(socket, SIGNAL(readyRead()), this, SLOT(readyRead()));
}

void TCPServer::acceptError()
{
    qWarning() << "WARNING: TCP client connection error:";
}

void TCPServer::socketError(QAbstractSocket::SocketError error)
{
    QTcpSocket * socket = qobject_cast<QTcpSocket *>(sender());
    if (socket == NULL) {
        qWarning() << "WARNING: Cannot obtain QTcpSocket* from QObject* in TCPServer::socketError(). This is a bug";
        return;
    }

    switch(error) {
        default:
            qWarning() << "WARNING: TCP client error:" << qPrintable(socket->errorString());
            break;
    }

    TCPConnection * conn = active_connections.value(socket);
    if (conn->socket == NULL) {
        goto deleteSocket;
    }

    // teardown

    active_connections.remove(socket);
    conn->upstream_request->disconnect();
    conn->upstream_request->deleteLater();
    delete conn;

    deleteSocket:

    socket->abort();
    socket->disconnect();
    socket->deleteLater();
}

void TCPServer::readyRead()
{
    QTcpSocket * socket = qobject_cast<QTcpSocket *>(sender());
    if (socket == NULL) {
        qWarning() << "WARNING: Cannot obtain QTcpSocket* from QObject* in TCPServer::readyRead(). This is a bug";
        return;
    }

    TCPConnection * conn = active_connections.value(socket);
    if (conn->socket == NULL) {
        qWarning() << "WARNING: No record of engaging in connection with client";
        return;
    }

    if (conn->request_size == 0) {
        if (socket->bytesAvailable() < (int)sizeof(quint16)) {
            return;
        }
        socket->read((char *)&(conn->request_size), sizeof(quint16));
        conn->request_size = qFromBigEndian(conn->request_size);
    }

    if (socket->bytesAvailable() < conn->request_size) {
        return;
    }

    conn->request_data = socket->readAll();
    emit haveRequest(socket);
}

void TCPServer::sendRequest(QTcpSocket * socket)
{
    TCPConnection * conn = active_connections.value(socket);
    if (conn->socket == NULL) {
        qWarning() << "WARNING: No record of engaging in connection with client";
        return;
    }

    DNSRequest * req = new DNSRequest(config, conn->request_data, socket);
    conn->upstream_request = req;
    connect(req, SIGNAL(haveAnswer(QTcpSocket*,QByteArray)), this, SLOT(getResponse(QTcpSocket*,QByteArray)));
    connect(req, SIGNAL(noAnswer(QTcpSocket*)), this, SLOT(noResponse(QTcpSocket*)));

    req->initiateRequest();
}

void TCPServer::sendResponse(QTcpSocket * socket)
{
    TCPConnection * conn = active_connections.value(socket);
    if (conn->socket == NULL) {
        qWarning() << "WARNING: No record of engaging in connection with client";
        return;
    }

    quint16 packet_size = (quint16)qToBigEndian((quint16)((conn->response_data).size()));
    socket->write((const char *)&packet_size, sizeof(quint16));
    socket->write((conn->response_data).data(), (conn->response_data).size());

    // start teardown

    active_connections.remove(socket);

    conn->upstream_request->disconnect();
    conn->upstream_request->deleteLater();

    socket->disconnectFromHost();
    socket->disconnect();
    connect(socket, SIGNAL(disconnected()), socket, SLOT(deleteLater()));

    delete conn;
}

void TCPServer::getResponse(QTcpSocket * socket, QByteArray data)
{
    TCPConnection * conn = active_connections.value(socket);
    if (conn->socket == NULL) {
        qWarning() << "WARNING: No record of engaging in connection with client";
        return;
    }

    conn->response_data = data;
    emit haveResponse(socket);
}

void TCPServer::noResponse(QTcpSocket * socket)
{
    TCPConnection * conn = active_connections.value(socket);
    if (conn->socket == NULL) {
        qWarning() << "WARNING: No record of engaging in connection with client";
        return;
    }

    // teardown

    active_connections.remove(socket);

    conn->upstream_request->disconnect();
    conn->upstream_request->deleteLater();

    socket->abort();
    socket->disconnect();
    socket->deleteLater();

    delete conn;
}
