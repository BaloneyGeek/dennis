// Copyright 2014 Boudhayan Gupta <me@BaloneyGeek.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef UTCPREQUEST_H
#define UTCPREQUEST_H

#include <QtEndian>
#include <QObject>
#include <QString>
#include <QByteArray>
#include <QList>
#include <QTimer>
#include <QUrl>
#include <QTcpSocket>

#include "DConfig.h"
#include "Common.h"

class UTCPRequest : public QObject
{
    Q_OBJECT

    private:

    QTcpSocket * socket;
    QTimer * timer;
    QByteArray data_upload;
    QByteArray data_download;
    DConfig * config;

    qint8   current_upstream_server;
    quint16 answer_size;
    quint64 socket_timeout;

    void nextServer();

    public:

    explicit UTCPRequest(DConfig * cfg, QByteArray data, QObject *parent = 0);
    ~UTCPRequest();

    void startRequesting();

    signals:

    void noAnswer();
    void haveAnswer(QByteArray);

    public slots:

    void readyWrite();
    void readyRead();
    void socketTimeout();
    void socketError(QAbstractSocket::SocketError error);
};

#endif
