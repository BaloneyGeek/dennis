// Copyright 2014 Boudhayan Gupta <me@BaloneyGeek.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef DNSREQUEST_H
#define DNSREQUEST_H

#include <QObject>
#include <QByteArray>
#include <QHostAddress>
#include <QTcpSocket>

#include "DConfig.h"
#include "UTCPRequest.h"
#include "Common.h"

struct DNSHeader {
    unsigned short id;       // unique id

    unsigned char rd     :1; // recursion desired
    unsigned char tc     :1; // truncated message
    unsigned char aa     :1; // authoritive answer
    unsigned char opcode :4; // purpose of message
    unsigned char qr     :1; // query/response flag

    unsigned char rcode  :4; // response code
    unsigned char cd     :1; // checking disabled
    unsigned char ad     :1; // authenticated data
    unsigned char z      :1; // reserved
    unsigned char ra     :1; // recursion available

    unsigned short n_ques;   // number of question entries
    unsigned short n_ans;    // number of answer entries
    unsigned short n_auth;   // number of authority entries
    unsigned short n_res;    // number of resource entries
};

struct DNSQuestion {
    unsigned short qtype;
    unsigned short qclass;
};

struct DNSAnswer {
    unsigned short atype;
    unsigned short aclass;
    unsigned int   ttl;
    unsigned short data_len;
};

class DNSRequest : public QObject
{
    Q_OBJECT

    private:

    QHostAddress request_sender;
    quint16      request_port;
    QTcpSocket * request_socket;
    QByteArray   request_data;

    enum {
        TCPSource,
        UDPSource
    } request_source;

    DConfig     * config;
    UTCPRequest * u_tcp_request;

    bool checkRequest();
    QByteArray createTruncatedEmptyResponse();

    public:

    explicit DNSRequest(DConfig * cfg, const QByteArray data, QHostAddress sender, quint16 port, QObject * parent = 0);
    explicit DNSRequest(DConfig * cfg, const QByteArray data, QTcpSocket * sock, QObject * parent = 0);
    ~DNSRequest();

    void initiateRequest();

    signals:

    void haveAnswer(DNSRequest *, QByteArray, QHostAddress, quint16);
    void haveAnswer(QTcpSocket *, QByteArray);
    void noAnswer(DNSRequest *);
    void noAnswer(QTcpSocket *);

    public slots:

    void haveUpstreamAnswer(QByteArray buffer);
    void noUpstreamAnswer();
};

#endif
