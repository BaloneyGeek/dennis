// Copyright 2014 Boudhayan Gupta <me@BaloneyGeek.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "UDPServer.h"

UDPServer::UDPServer(DConfig * cfg, QObject * parent) : QObject(parent)
{
    config = cfg;

    socket = new QUdpSocket(this);
    if (!(socket->bind(config->host(), config->port()))) {
        qFatal("ERROR: Failed to bind UDP server to port %d on server %s", config->port(), qPrintable(config->host().toString()));
    }
    connect(socket, SIGNAL(readyRead()), this, SLOT(readyRead()));

    qDebug() << "Listening for UDP connections on port" << config->port() << "on IP" << qPrintable(config->host().toString());
}

void UDPServer::readyRead()
{
    QByteArray buffer;
    QHostAddress sender;

    DNSRequest * req;
    quint16 port;

    buffer.resize(socket->pendingDatagramSize());
    socket->readDatagram(buffer.data(), buffer.size(), &sender, &port);

    req = new DNSRequest(config, buffer, sender, port);
    connect(req, SIGNAL(haveAnswer(DNSRequest*,QByteArray,QHostAddress,quint16)), this, SLOT(sendBackAnswer(DNSRequest*,QByteArray,QHostAddress,quint16)));
    connect(req, SIGNAL(noAnswer(DNSRequest*)), this, SLOT(noAnswer(DNSRequest*)));

    upstream_requests.insert(req);
    req->initiateRequest();
}

void UDPServer::sendBackAnswer(DNSRequest * req, QByteArray buffer, QHostAddress host, quint16 port)
{
    socket->writeDatagram(buffer.data(), buffer.size(), host, port);
    upstream_requests.remove(req);

    req->disconnect();
    req->deleteLater();
}

void UDPServer::noAnswer(DNSRequest * req)
{
    upstream_requests.remove(req);

    req->disconnect();
    req->deleteLater();
}
