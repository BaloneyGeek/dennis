// Copyright 2014 Boudhayan Gupta <me@BaloneyGeek.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "DNSRequest.h"

DNSRequest::DNSRequest(DConfig * cfg, const QByteArray data, QHostAddress sender, quint16 port, QObject * parent) : QObject(parent)
{
    config = cfg;

    request_data   = data;
    request_sender = sender;
    request_port   = port;
    request_source = UDPSource;
}

DNSRequest::DNSRequest(DConfig * cfg, const QByteArray data, QTcpSocket * sock, QObject * parent) : QObject(parent)
{
    config = cfg;

    request_data   = data;
    request_socket = sock;
    request_source = TCPSource;
}

DNSRequest::~DNSRequest()
{
    ;;
}

void DNSRequest::initiateRequest()
{
    if(!(checkRequest())) {
        QHostAddress ha;

        switch(request_source) {
            case UDPSource:
                ha = request_sender;
                break;
            case TCPSource:
                ha = request_socket->peerAddress();
                break;
        }
        qWarning() << "WARNING: Recieved malformed DNS request from" << qPrintable(ha.toString());

        switch (request_source) {
            case UDPSource:
                emit noAnswer(this);
                break;
            case TCPSource:
                emit noAnswer(request_socket);
                break;
        }
        return;
    }

    u_tcp_request = new UTCPRequest(config, request_data);

    connect(u_tcp_request, SIGNAL(haveAnswer(QByteArray)), this, SLOT(haveUpstreamAnswer(QByteArray)));
    connect(u_tcp_request, SIGNAL(noAnswer()), this, SLOT(noUpstreamAnswer()));

    u_tcp_request->startRequesting();
}

bool DNSRequest::checkRequest()
{
    quint32 request_size = request_data.size();
    char * dataptr = request_data.data();

    if (request_size < 12) {
        return false;
    }

    if((dataptr[2] & 0x80) != 0x00)
    {
        return false;
    }

    return true;
}

QByteArray DNSRequest::createTruncatedEmptyResponse()
{
    DNSHeader * question_header = (DNSHeader *)request_data.data();
    DNSHeader answer_header;
    QByteArray full_response;

    char * ptr;

    answer_header.id = question_header->id;
    answer_header.rd = question_header->rd;
    answer_header.tc = 1;
    answer_header.aa = 0;
    answer_header.opcode = 0;
    answer_header.qr = 1;
    answer_header.rcode = 0;
    answer_header.cd = 0;
    answer_header.ad = 0;
    answer_header.z = 0;
    answer_header.ra = 1;

    answer_header.n_ques = question_header->n_ques;
    answer_header.n_ans = 0;
    answer_header.n_auth = 0;
    answer_header.n_res = 0;

    full_response.append((const char *)&answer_header, sizeof(answer_header));

    ptr = (char *)question_header;
    ptr += sizeof(DNSHeader);
    full_response.append(ptr, request_data.size() - sizeof(DNSHeader));

    return full_response;
}

void DNSRequest::haveUpstreamAnswer(QByteArray buffer)
{
    switch(request_source) {
        case UDPSource:
            if (buffer.size() <= 512)
                emit haveAnswer(this, buffer, request_sender, request_port);
            else
                emit haveAnswer(this, createTruncatedEmptyResponse(), request_sender, request_port);
            break;
        case TCPSource:
            emit haveAnswer(request_socket, buffer);
            break;
    }

    u_tcp_request->disconnect();
    u_tcp_request->deleteLater();
}

void DNSRequest::noUpstreamAnswer()
{
    switch (request_source) {
        case UDPSource:
            emit noAnswer(this);
            break;
        case TCPSource:
            emit noAnswer(request_socket);
            break;
    }

    u_tcp_request->disconnect();
    u_tcp_request->deleteLater();
}
