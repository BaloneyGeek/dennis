// Copyright 2014 Boudhayan Gupta <me@BaloneyGeek.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <QCoreApplication>

#include "DConfig.h"
#include "UDPServer.h"
#include "TCPServer.h"
#include "Common.h"

UDPServer * udpserver = NULL;
TCPServer * tcpserver = NULL;

int main(int argc, char * argv[])
{
    QCoreApplication a(argc, argv);

    qDebug() << "Dennis-NG - A UDP-TCP DNS Proxy";
    qDebug() << "Version" << DENNIS_VER << "/ Qt" << qVersion();
    qDebug() << "";

    DConfig cfg(QStringLiteral("/etc/dennis.json"));

    if (cfg.enableDownUdp()) udpserver = new UDPServer(&cfg);
    if (cfg.enableDownTcp()) tcpserver = new TCPServer(&cfg);

    a.exec();

    delete udpserver;
    delete tcpserver;
}
