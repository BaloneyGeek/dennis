// Copyright 2014 Boudhayan Gupta <me@BaloneyGeek.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef TCPSERVER_H
#define TCPSERVER_H

#include <QObject>
#include <QtEndian>
#include <QMap>
#include <QTcpServer>
#include <QTcpSocket>

#include "DConfig.h"
#include "DNSRequest.h"
#include "Common.h"

struct TCPConnection {
    QTcpSocket * socket;
    QByteArray   request_data;
    QByteArray   response_data;
    quint16      request_size;
    DNSRequest * upstream_request;

    TCPConnection();
};

class TCPServer : public QObject
{
    Q_OBJECT

    private:

    DConfig * config;
    QTcpServer * server;
    QMap<QTcpSocket *, TCPConnection *> active_connections;

    public:

    explicit TCPServer(DConfig * cfg, QObject *parent = 0);
    ~TCPServer();

    signals:

    void haveRequest(QTcpSocket *);
    void haveResponse(QTcpSocket *);

    public slots:

    void newConnection();
    void acceptError();
    void socketError(QAbstractSocket::SocketError error);
    void sendRequest(QTcpSocket * socket);
    void sendResponse(QTcpSocket * socket);
    void readyRead();
    void getResponse(QTcpSocket * socket, QByteArray data);
    void noResponse(QTcpSocket * socket);
};

#endif
