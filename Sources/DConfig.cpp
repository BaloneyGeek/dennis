// Copyright 2014 Boudhayan Gupta <me@BaloneyGeek.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "DConfig.h"

DConfig::DConfig(const QString & cfgfile)
{
    QFile cfg_file(cfgfile);
    if (!cfg_file.open(QIODevice::ReadOnly)) {
        qFatal("Cannot open configuration file for reading\n");
    }

    QJsonValue temp;

    QByteArray cfgdata = cfg_file.readAll();
    cfg_file.close();
    QJsonDocument json_d = QJsonDocument::fromJson(cfgdata);
    QJsonObject json = json_d.object();

    temp = json["request_timeout"];
    mTimeout = temp.toInt(5);

    temp = json["listen_host"];
    mHost.setAddress(temp.toString(QStringLiteral("127.0.0.1")));

    temp = json["listen_port"];
    mPort = temp.toInt(53);

    temp = json["enable_downstream_tcp"];
    mEnableDownstreamTCP = temp.toBool(true);

    temp = json["enable_downstream_udp"];
    mEnableDownstreamUDP = temp.toBool(true);

    QJsonArray ups_array = json["upstream_servers"].toArray();
    for (quint8 i = 0; i < ups_array.size(); i++) {
        QString us = ups_array[i].toString();
        mUpstreams.append(QUrl(us));
    }
}

QHostAddress & DConfig::host()      { return mHost; }
quint16 DConfig::port() const       { return mPort; }
QList<QUrl> & DConfig::upstreams()  { return mUpstreams; }
quint8 DConfig::timeout() const     { return mTimeout; }
bool DConfig::enableDownUdp() const { return mEnableDownstreamUDP; }
bool DConfig::enableDownTcp() const { return mEnableDownstreamTCP; }
