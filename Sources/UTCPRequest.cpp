// Copyright 2014 Boudhayan Gupta <me@BaloneyGeek.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "UTCPRequest.h"

UTCPRequest::UTCPRequest(DConfig * cfg, QByteArray data, QObject *parent) : QObject(parent)
{
    config = cfg;
    current_upstream_server = -1;

    socket = new QTcpSocket(this);
    connect(socket, SIGNAL(connected()), this, SLOT(readyWrite()));
    connect(socket, SIGNAL(readyRead()), this, SLOT(readyRead()));
    connect(socket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(socketError(QAbstractSocket::SocketError)));

    timer = new QTimer(this);
    timer->setSingleShot(true);
    connect(timer, SIGNAL(timeout()), this, SLOT(socketTimeout()));

    data_upload = data;
    socket_timeout = (config->timeout() * 1000); // seconds to milliseconds
    answer_size = 0;
}

UTCPRequest::~UTCPRequest()
{
    socket->abort();
    socket->disconnect();
    delete socket;

    timer->stop();
    timer->disconnect();
    delete timer;
}

void UTCPRequest::nextServer()
{
    // check if there really is a next server

    current_upstream_server++;
    if (current_upstream_server >= config->upstreams().size()) {
        emit noAnswer();
        socket->abort();
        return;
    }

    // abort the current attempt and reconnect to the next server

    socket->abort();
    QString host = (config->upstreams().at(current_upstream_server)).host();
    quint16 port = (config->upstreams().at(current_upstream_server)).port();

    socket->connectToHost(host, port);
}

void UTCPRequest::startRequesting()
{
    return nextServer();
}

void UTCPRequest::readyWrite()
{
    // first we push the data out

    quint16 packet_size = (quint16)qToBigEndian((quint16)data_upload.size());
    socket->write((const char *)&packet_size, sizeof(quint16));
    socket->write(data_upload.data(), data_upload.size());

    // then we start the timer

    timer->start(socket_timeout);
}

void UTCPRequest::readyRead()
{
    // first we disable the timer

    timer->stop();

    // then we attempt a read. we restart the timer before we return
    // every time so that a slow connection eventually times out

    if (answer_size == 0) {
        if (socket->bytesAvailable() < (int)sizeof(quint16)) {
            timer->start(socket_timeout);
            return;
        }
        socket->read((char *)&answer_size, sizeof(quint16));
        answer_size = qFromBigEndian(answer_size);
    }

    if (socket->bytesAvailable() < answer_size) {
        timer->start(socket_timeout);
        return;
    }

    // at this point, we are sure we can read the entire reply, so
    // we grab the data and emit the haveAnswer signal

    data_download = socket->readAll();
    emit haveAnswer(data_download);
}

void UTCPRequest::socketTimeout()
{
    qWarning() << "WARNING: Connection to upstream server timed out";
    return nextServer();
}

void UTCPRequest::socketError(QAbstractSocket::SocketError error)
{
    qWarning() << "WARNING: Error connecting to upstream server";
    switch (error) {
        case QAbstractSocket::RemoteHostClosedError:
            qWarning() << " " << "The connection was closed by the upstream server";
            break;
        case QAbstractSocket::ConnectionRefusedError:
            qWarning() << " " << "The connection was refused by the upstream server";
            break;
        default:
            qWarning() << " " << qPrintable(socket->errorString());
    }
    return nextServer();
}
